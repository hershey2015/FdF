/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/11 14:23:38 by coleksii          #+#    #+#             */
/*   Updated: 2017/05/24 18:21:24 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "minilibx_macos/mlx.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>


int	print_line(int x1, int y1, int x2, int y2, void **mw)
{
	int p_x;
	int p_y;
	int y = x2;
	int x = y2;
	int col = 0xffff4d;

	p_x = (x1 - x2 >= 0) ? x1 - x2 : x2 - x1;
	p_y = (y1 - y2 >= 0) ? y1 - y2 : y2 - y1;
	if (p_x >= p_y)
	{
	write(1, "AAA", 3);
		if (x1 >= x2 && write(1, "111", 3))
			while (x <= x1)
			{
				mlx_pixel_put(mw[0], mw[1], x++, y, col);
				y = ((x - x1) * (y2 - y1) / (x2 - x1)) + y1;
			}
		else if (write(1, "222", 3))
			while (x >= x1)
			{
				mlx_pixel_put(mw[0], mw[1], x--, y, col);
				y = ((x - x1) * (y2 - y1) / (x2 - x1)) + y1;
			}
	}
	else
	{
	write(1, "BBB", 3);
		if (y1 >= y2 && write(1, "111", 3))
			while (y <= y1)
			{
				mlx_pixel_put(mw[0], mw[1], x, y++, col);
				x = ((y - y1) * (x2 - x1) / (y2 - y1)) + x1;
			}
		else if (write(1, "222", 3))
			while (y >= y1)
			{
				mlx_pixel_put(mw[0], mw[1], x, y--, col);
				x = ((y - y1) * (x2 - x1) / (y2 - y1)) + x1;
			}
	}
	return (0);
}

int	khook(int key, void **mw)
{
	int x = 200;
	int y = 200;
	
	if (key == 126)
	{
		while (x <= 400)
			mlx_pixel_put(mw[0], mw[1], x++, y, 0x008FFFFF);
		while (y <= 400)
			mlx_pixel_put(mw[0], mw[1], x, y++, 0x008FFFFF);
		while (x >= 200)
			mlx_pixel_put(mw[0], mw[1], x--, y, 0x008FFFFF);
		while (y >= 200)
			mlx_pixel_put(mw[0], mw[1], x, y--, 0x008FFFFF);
	}
	if (key == 125)
	{
		x = 0;
		y = 0;
		while (y <= 800)
		{
			x = 0;
			while (x <= 800)
				mlx_pixel_put(mw[0], mw[1], x++, y, 0x000);
			y++;
		}
	}
	printf ("\n key == %d\n", key);
	return (0);
}

int mous(int key, int x, int y, void **mw)
{
	int x1 = 400;
	int y1 = 400;
	print_line(x, y, x1, y1, mw);
	printf("\nmouse = %d\n", key);
	return (0);
}

int expo(int key, void **mv)
{
	printf("\nexp = %d\n", key); //переключение окон 
	return (0);
}

int main()
{
	void	*mlx;
	void	*win;
	void	**mw = (void **)malloc(sizeof(void *) * 3);
	int		x;
	int		y;
	int		color;
	y = 200;
	x = 200;
	mlx = mlx_init();
	win = mlx_new_window(mlx, 800, 800, "mlx 42");
	mw[0] = mlx;
	mw[1] = win;
	while (x <= 400)
		mlx_pixel_put(mlx, win, x++, y, 0x00FFFFFF);
	while (y <= 400)
		mlx_pixel_put(mlx, win, x, y++, 0x00FFFFFF);
	while (x >= 200)
		mlx_pixel_put(mlx, win, x--, y, 0x00FFFFFF);
	while (y >= 200)
		mlx_pixel_put(mlx, win, x, y--, 0x00FFFFFF);
	color = 0x000FFFFF;

	int a;
	a = 0;

	y = 1;
	x = 1;
	while (y <= 800 && y >= 0 && x <= 800 && x >= 0)
	{
		mlx_pixel_put(mlx, win, x, y, 0x00FFFFFF);
		y = x * x;
		x++;
	}
	x = 100;
	y = 200;
	a = 300;
	int r = 199;
	int b = 300;
	while (r++ < 300 && (color += 1))
	{
		while (y <= 800 && y >= 0 && x <= 800 && x <= 500)
		{
			mlx_pixel_put(mlx, win, x, y, color);
			y = sqrt((r * r) - ((x - a) * (x - a))) + b;
			x++;
		}
		while ( x >= 100)
		{
			mlx_pixel_put(mlx, win, x, y, color);
			y = -sqrt((r * r) - ((x - a) * (x - a))) + b;
			x--;
		}
	}
	mw[2] = &color;
	mlx_mouse_hook(win, mous, mw);
	mlx_key_hook(win, khook, mw);
	mlx_expose_hook (win, expo, mw);
	//	mlx_hook (win, 0, 2, khook, mw);

	mlx_loop(mlx);
	return (0);
}
