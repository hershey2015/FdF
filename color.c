/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/26 19:24:39 by coleksii          #+#    #+#             */
/*   Updated: 2017/05/26 19:33:09 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int		color_max(t_p p, int *z)
{
	int rgb_1[3];
	int rgb_2[3];
	int r[3];

	rgb_2[0] = 0xff;
	rgb_2[1] = 0xff;
	rgb_2[2] = 0xff;
	if (p.z > 0)
	{
		rgb_1[0] = 0xff;
		rgb_1[1] = 0;
		rgb_1[2] = 0xff;
	}
	else if (p.z < 0)
	{
		rgb_1[0] = 0;
		rgb_1[1] = 0;
		rgb_1[2] = 0xff;
	}
	else
		return (0xffffff);
	r[0] = p.z / ((p.z > 0) ? z[0] : z[1]) * (rgb_1[0] - rgb_2[0]) + rgb_1[0];
	r[1] = p.z / ((p.z > 0) ? z[0] : z[1]) * (rgb_1[1] - rgb_2[1]) + rgb_1[1];
	r[2] = p.z / ((p.z > 0) ? z[0] : z[1]) * (rgb_1[2] - rgb_2[2]) + rgb_1[2];
	return ((r[0] << 16) + (r[1] << 8) + r[2]);
}

int		color_all(int a, double *delta, int color1, int color2)
{
	int rgb_1[3];
	int rgb_2[3];
	int rgb[3];

	rgb_1[0] = (color1 & 0xff0000) >> 16;
	rgb_1[1] = (color1 & 0xff00) >> 8;
	rgb_1[2] = (color1 & 0xff);
	rgb_2[0] = (color2 & 0xff0000) >> 16;
	rgb_2[1] = (color2 & 0xff00) >> 8;
	rgb_2[2] = (color2 & 0xff);
	rgb[0] = a / (delta[0] - delta[1]) * (rgb_1[0] - rgb_2[0]) + rgb_1[0];
	rgb[1] = a / (delta[0] - delta[1]) * (rgb_1[1] - rgb_2[1]) + rgb_1[1];
	rgb[2] = a / (delta[0] - delta[1]) * (rgb_1[2] - rgb_2[2]) + rgb_1[2];
	return ((rgb[0] << 16) + (rgb[1] << 8) + rgb[2]);
}

int		color_x(t_p p1, t_p p2, int x, int *z)
{
	int		color;
	int		color1;
	int		color2;
	double	delta[2];

	color1 = (p1.col == 0) ? color_max(p1, z) : p1.col;
	color2 = (p2.col == 0) ? color_max(p2, z) : p2.col;
	x -= p1.x;
	delta[0] = p1.x;
	delta[1] = p2.x;
	color = color_all(x, delta, color1, color2);
	z = 0;
	return (color);
}

int		color_y(t_p p1, t_p p2, int y, int *z)
{
	int		color;
	int		color1;
	int		color2;
	double	delta[2];

	color1 = (p1.col == 0) ? color_max(p1, z) : p1.col;
	color2 = (p2.col == 0) ? color_max(p2, z) : p2.col;
	y -= p1.y;
	delta[0] = p1.y;
	delta[1] = p2.y;
	color = color_all(y, delta, color1, color2);
	z = 0;
	return (color);
}
