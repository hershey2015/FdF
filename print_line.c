/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_line.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/18 17:44:59 by coleksii          #+#    #+#             */
/*   Updated: 2017/05/26 20:31:54 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

void	for_x(t_p p1, t_p p2, void **mw)
{
	double y;
	double x;

	y = p2.y;
	x = p2.x;
	if (p1.x >= p2.x)
		while (x <= p1.x)
		{
			if (x >= 0 && x <= 1200 && y >= 0 && y <= 1200)
				mlx_pixel_put(mw[0], mw[1], x, y, color_x(p1, p2, x, mw[7]));
			x++;
			y = ((x - p1.x) * (p2.y - p1.y) / (p2.x - p1.x)) + p1.y;
		}
	else
		while (x >= p1.x)
		{
			if (x >= 0 && x <= 1200 && y >= 0 && y <= 1200)
				mlx_pixel_put(mw[0], mw[1], x, y, color_x(p2, p1, x, mw[7]));
			x--;
			y = ((x - p1.x) * (p2.y - p1.y) / (p2.x - p1.x)) + p1.y;
		}
}

void	for_y(t_p p1, t_p p2, void **mw)
{
	double y;
	double x;

	y = p2.y;
	x = p2.x;
	if (p1.y >= p2.y)
		while (y <= p1.y)
		{
			if (x >= 0 && x <= 1200 && y >= 0 && y <= 1200)
				mlx_pixel_put(mw[0], mw[1], x, y, color_y(p1, p2, y, mw[7]));
			y++;
			x = ((y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y)) + p1.x;
		}
	else
		while (y >= p1.y)
		{
			if (x >= 0 && x <= 1200 && y >= 0 && y <= 1200)
				mlx_pixel_put(mw[0], mw[1], x, y, color_y(p2, p1, y, mw[7]));
			--y;
			x = ((y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y)) + p1.x;
		}
}

int		print_line(t_p p1, t_p p2, void **mw)
{
	double p_x;
	double p_y;

	p_x = (p1.x - p2.x >= 0) ? p1.x - p2.x : p2.x - p1.x;
	p_y = (p1.y - p2.y >= 0) ? p1.y - p2.y : p2.y - p1.y;
	if (p_x >= p_y)
		for_x(p1, p2, mw);
	else
		for_y(p1, p2, mw);
	return (0);
}
