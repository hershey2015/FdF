/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   head.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 16:15:53 by coleksii          #+#    #+#             */
/*   Updated: 2017/05/26 20:54:26 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEAD_H
# define HEAD_H

# include "minilibx_macos/mlx.h"
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <math.h>
# include <fcntl.h>

typedef struct	s_list
{
	double	x;
	double	y;
	double	z;
	int		col;
}				t_p;
int				khook(int key, void **mw);
void			corr(t_p ***point, char *s, int h);
int				ft_printf(const char *format, ...);
int				printer(t_p ***point, int h, int l, int *z);
int				print_line(t_p p1, t_p p2, void **mw);
int				contin(t_p ***p, int index, void **mv);
int				turn_y(t_p ***p, void **mw, double *ang);
void			pred_index(t_p *dot1, t_p *dot2, void **mw, int i);
int				color_y(t_p p1, t_p p2, int y, int *z);
int				color_x(t_p p1, t_p p2, int x, int *z);

#endif
