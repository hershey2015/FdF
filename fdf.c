/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 16:10:26 by coleksii          #+#    #+#             */
/*   Updated: 2017/05/26 21:11:00 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int	ft_read(int i, char **av, int h)
{
	char	buf[i];
	int		file;
	int		fd;
	t_p		***point;

	point = (t_p ***)malloc(sizeof(t_p **) * (h + 1));
	point[h] = 0;
	point[1] = point[1];
	fd = open(av[1], O_RDONLY);
	file = read(fd, buf, i);
	buf[file] = '\0';
	printf("h = %d\n", h);
	printf("%s", buf);
	corr(point, buf, h);
	free(point);
	return (0);
}

int	main(int ar, char **av)
{
	int		fd;
	char	b;
	int		i;
	int		h;

	i = 0;
	h = 0;
	if (ar != 2)
		return (0);
	fd = open(av[1], O_RDONLY);
	if (fd == -1 && write(1, "dont file", 9))
		return (0);
	while (read(fd, &b, 1))
	{
		if (b == '\n')
			++h;
		i++;
	}
	close(fd);
	ft_read(i, av, h);
	return (0);
}
