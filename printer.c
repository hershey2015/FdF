/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printer.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/18 16:50:18 by coleksii          #+#    #+#             */
/*   Updated: 2017/05/26 18:00:49 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"


int	ind(int h, int l, void **mw)
{
	int	p;
	int *z;

	z = (int *)mw[7];
	z[2] = 600;
	z[3] = 600;
	p = (h > l) ? 1000.00 / (h - 1) : 1000.00 / (l - 1);
	return (p);
}

int		printer(t_p ***point, int h, int l, int *z)
{
	void	*mlx;
	void	*win;
	void	**mw;
	int	i;
	double	ang[3];

	mw = (void **)malloc(sizeof(void *) * 7);
	ang[0] = 60;
	ang[1] = 10;
	ang[2] = 45;
	mw[7] = z;
	i = ind(l, h, mw);
	mlx = mlx_init();
	win = mlx_new_window(mlx, 1200, 1200, "mlx 42");
	mw[0] = mlx;
	mw[1] = win;
	mw[2] = point;
	mw[3] = &i;
	mw[4] = ang;
	mw[5] = &h;
	mw[6] = &l;
	turn_y(point, mw, ang);
	mlx_key_hook(win, khook, mw);
	mlx_loop(mlx);
	return (0);
}
