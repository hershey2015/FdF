# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: coleksii <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/05/28 15:27:44 by coleksii          #+#    #+#              #
#    Updated: 2017/05/28 15:41:00 by coleksii         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = @gcc

NAME = fdf

OBJ = fdf.o correct.o printer.o print_line.o turn.o hook.o color.o

CFLAGS = -Wall -Wextra -Werror

HEADER = head.h

all: $(NAME)

$(NAME): $(OBJ)
	@gcc -o $(NAME) -I $(HEADER) $(OBJ) -lmlx -framework OpenGL -framework AppKit
	@echo "\033[32;1m<<done>>\033[0m"
clean:
	@rm -f $(OBJ)
	@echo "\033[32;1m<<delete objects>>\033[0m"

fclean: clean
	@rm -rf $(NAME)
	@echo "\033[32;1m<<delete fdf>>\033[0m"

re: fclean all
	@echo "\033[32;1m<<re succes>>\033[0m"

