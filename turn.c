/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   turn.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/21 14:09:07 by coleksii          #+#    #+#             */
/*   Updated: 2017/05/26 21:07:06 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define A angl[0]
#define B angl[1]
#define C angl[2]
#include "head.h"

void	pred(t_p *dot1, t_p *dot2, void **mw, int i)
{
	t_p p1;
	t_p p2;

	p1.x = dot1->x * i + ((int *)mw[7])[2];
	p1.y = dot1->y * i + ((int *)mw[7])[3];
	p1.z = dot1->z;
	p2.x = dot2->x * i + ((int *)mw[7])[2];
	p2.y = dot2->y * i + ((int *)mw[7])[3];
	p2.z = dot2->z;
	p1.col = dot1->col;
	p2.col = dot2->col;
	print_line(p1, p2, mw);
}

void	f_xyz(double *xyz, void **mw)
{
	xyz[2] = *((int *)mw[7]) - 1;
	xyz[2] = xyz[2] / 2;
	xyz[0] = *((int *)mw[6]) - 1;
	xyz[0] = xyz[0] / 2;
	xyz[1] = *((int *)mw[5]) - 1;
	xyz[1] = xyz[1] / 2;
}

int		turning_y(t_p *dot1, t_p *dot2, void **mw, double *ang)
{
	double	xyz[3];
	double	angl[3];
	t_p		p1;
	t_p		p2;

	angl[1] = ang[1] * 0.017453292519943;
	angl[0] = ang[0] * 0.017453292519943;
	angl[2] = ang[2] * 0.017453292519943;
	f_xyz(xyz, mw);
	p1 = *dot1;
	p2 = *dot2;
	p1.x -= xyz[0];
	p1.y -= xyz[1];
	p2.x -= xyz[0];
	p2.y -= xyz[1];
	p1.x = cos(C) * cos(B) * p1.x - cos(B) * sin(C) * p1.y + sin(B) * p1.z;
	p1.y = (sin(A) * sin(B) * cos(C) + cos(A) * sin(C)) * (dot1->x - xyz[0])
		+ (sin(A) * sin(B) * (-sin(C)) + cos(A) * cos(C)) * p1.y
		- (sin(A) * cos(B)) * p1.z;
	p2.x = cos(C) * cos(B) * p2.x - cos(B) * sin(C) * p2.y + sin(B) * p2.z;
	p2.y = (sin(A) * sin(B) * cos(C) + cos(A) * sin(C)) * (dot2->x - xyz[0])
		+ (sin(A) * sin(B) * (-sin(C)) + cos(A) * cos(C)) * p2.y
		- (sin(A) * cos(B)) * p2.z;
	pred(&p1, &p2, mw, *((int *)mw[3]));
	return (0);
}

int		turn_y(t_p ***p, void **mw, double *ang)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (p[i] != 0)
	{
		j = 0;
		while (p[i][j] != 0)
		{
			if (p[i][j + 1])
				turning_y(p[i][j], p[i][j + 1], mw, ang);
			if (p[i + 1] && p[i + 1][j])
				turning_y(p[i][j], p[i + 1][j], mw, ang);
			++j;
		}
		++i;
	}
	printf("\360\237\230\204\n");
	printf("\n");
	return (0);
}
