/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/26 14:55:27 by coleksii          #+#    #+#             */
/*   Updated: 2017/05/26 19:23:56 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

void	clean_map(void **mw)
{
	int x;
	int y;

	x = 0;
	y = 0;
	while (y <= 1200)
	{
		x = 0;
		while (x <= 1200)
			mlx_pixel_put(mw[0], mw[1], x++, y, 0x000);
		y++;
	}
}

void	rotation(int key, double *an)
{
	if (key == 124)
		an[1] = (an[1] > 350) ? an[1] - 350 : an[1] + 10;
	if (key == 123)
		an[1] = (an[1] < 10) ? an[1] + 350 : an[1] - 10;
	if (key == 125)
		an[0] = (an[0] < 10) ? an[0] + 350 : an[0] - 10;
	if (key == 126)
		an[0] = (an[0] < 350) ? an[0] - 350 : an[0] + 10;
	if (key == 88)
		an[2] = (an[2] < 350) ? an[2] - 350 : an[2] + 10;
	if (key == 86)
		an[2] = (an[0] < 10) ? an[2] + 350 : an[2] - 10;
}

int		khook(int key, void **mw)
{
	t_p		***point;
	int		*index;
	double	*an;

	point = (t_p ***)mw[2];
	index = (int *)mw[3];
	an = (double *)mw[4];
	rotation(key, an);
	(key == 78) ? *index /= 1.3 : 0;
	(key == 69) ? *index *= 1.3 : 0;
	(key == 13) ? ((int *)mw[7])[3] -= 50 : 0;
	(key == 1) ? ((int *)mw[7])[3] += 50 : 0;
	(key == 0) ? ((int *)mw[7])[2] -= 50 : 0;
	(key == 2) ? ((int *)mw[7])[2] += 50 : 0;
	clean_map(mw);
	turn_y(point, mw, an);
	return (0);
}
