/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   correct.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 13:43:24 by coleksii          #+#    #+#             */
/*   Updated: 2017/05/26 20:52:05 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int		count(char *s, int *i)
{
	int l;
	int flag;

	l = 0;
	flag = 0;
	while (s[*i] != '\n' && s[*i] != '\0')
	{
		if (s[*i] != '-' && (s[*i] < 48 || s[*i] > 57) && s[*i] != ' '
			&& s[*i] != ',' && s[*i] != 'x' && (s[*i] < 65 || s[*i] > 70))
			return (-1);
		if (s[*i] == ' ')
			flag = 0;
		if (s[*i] == '-' && s[*i + 1] > 47 && s[*i + 1] < 58)
			++*i;
		else if (s[*i] == '-')
			return (-1);
		if (s[*i] > 47 && s[*i] < 58 && flag == 0 && ++l)
			flag = 1;
		++*i;
	}
	++*i;
	return (l);
}

int		read_color(char **s, t_p *lst)
{
	++*s;
	if (**s != '0' && *(*s + 1) != 'x')
		return (0);
	*s += 2;
	while ((**s > 47 && **s < 58) || (**s > 64 && **s < 71))
	{
		lst->col = (lst->col * 16) + ((**s <= '9') ? **s - '0' : **s - 55);
		++*s;
	}
	return (1);
}

t_p		*filling(char **s, int j, int l, int *z)
{
	t_p	*lst;
	int minus;

	minus = 1;
	lst = (t_p *)malloc(sizeof(t_p));
	lst->x = l;
	lst->y = j;
	lst->col = 0;
	while (**s == ' ')
		++*s;
	if (**s == '-' && ++*s)
		minus = -1;
	lst->z = 0;
	while (**s > 47 && **s < 58)
	{
		lst->z = (lst->z * 10) + (**s - 48);
		++*s;
	}
	if (**s == ',' && !(read_color(s, lst)))
		return (0);
	lst->z *= minus;
	z[2] = (z[2] < lst->z) ? lst->z : z[2];
	z[4] = (z[4] > lst->z) ? lst->z : z[4];
	return (lst);
}

void	kost(int *l, char *s, int *j, t_p ***point)
{
	l[0] = 0;
	while (l[3]--)
	{
		point[*j][l[0]] = filling(&s, *j, l[0], l);
		++l[0];
	}
	l[1] = (l[0] > l[1]) ? l[0] : l[1];
	point[*j][l[0]] = 0;
	++*j;
}

void	corr(t_p ***point, char *s, int h)
{
	int		j;
	int		i;
	int		l[5];
	int		z[4];
	char	*s1;

	j = 0;
	l[1] = 0;
	l[4] = 0;
	while (j < h)
	{
		i = 0;
		l[3] = count(s, &i);
		s1 = s;
		if (l[3] == -1 && write(1, "\nnot valid map\n", 16))
			return ;
		point[j] = (t_p **)malloc(sizeof(t_p *) * (l[3] + 1));
		point[j][l[3]] = 0;
		kost(l, s, &j, point);
		s = &s1[i];
	}
	z[0] = l[2];
	z[1] = l[4];
	printer(point, h, l[1], z);
}
